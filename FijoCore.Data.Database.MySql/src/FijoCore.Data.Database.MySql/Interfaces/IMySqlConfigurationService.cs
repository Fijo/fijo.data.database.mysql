using FijoCore.Data.Database.MySql.Dto;

namespace FijoCore.Data.Database.MySql.Interfaces {
	public interface IMySqlConfigurationService {
		string GetConnectionString(MySqlConfiguration configuration);
	}
}