using MySql.Data.MySqlClient;

namespace FijoCore.Data.Database.MySql.Interfaces {
	public interface IMySqlQueryService {
		long Insert(string mySqlQueryString);
	    bool Remove(string mySqlQueryString);
		MySqlDataReader Query(string mySqlQueryString);
	}
}