using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;

namespace FijoCore.Data.Database.MySql.Interfaces
{
    public interface IDatabaseKeyRepository : IRepository<IDictionary<string, string>>
    {
        
    }
}