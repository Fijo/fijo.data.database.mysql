using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;

namespace FijoCore.Data.Database.MySql.Interfaces {
    public interface IMySqlRepository<TEntity, TKey> : ICollectionRepository<TEntity>
    {
        TKey Set(TEntity entity);
        void Set(IList<TEntity> collection);
        bool Remove(TKey key);
        void Remove(IList<TKey> collection);
		TEntity Get(TKey id);
        IEnumerable<TEntity> Get(IList<TKey> collection);
	}
}