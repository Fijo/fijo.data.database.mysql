﻿using System.Collections.Generic;

namespace FijoCore.Data.Database.MySql.Interfaces
{
    public interface ISqlQueryBuilder
    {
        string GetQueryAllSql();
        string GetQueryByIdsSql(IEnumerable<string> keys);
        string GetInsertOrUpdateSql(IEnumerable<string> values);
        string GetRemoveSql(IEnumerable<string> keys);
    }
}