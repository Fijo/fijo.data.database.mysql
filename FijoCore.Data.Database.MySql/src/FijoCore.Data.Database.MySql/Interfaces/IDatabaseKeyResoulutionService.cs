namespace FijoCore.Data.Database.MySql.Interfaces
{
    public interface IDatabaseKeyResoulutionService
    {
        string Resolve(string databaseKey);
    }
}