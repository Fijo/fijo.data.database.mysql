using System.Collections.Generic;
using FijoCore.Data.Database.MySql.Dto;

namespace FijoCore.Data.Database.MySql.Interfaces
{
    public interface IEntityFieldProvider
    {
        IList<EntityField> GetEntityFields();
        object CreateInstance();
        EntityField GetPrimaryKey();
        string GetTableName();
        string GetDatabase();
    }
}