﻿using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Data.Database.MySql.Interfaces;
using FijoCore.Data.Database.MySql.Repository;
using FijoCore.Data.Database.MySql.Services;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using MySql.Data.MySqlClient;
using MySqlConfiguration = FijoCore.Data.Database.MySql.Dto.MySqlConfiguration;

namespace FijoCore.Data.Database.MySql.Properties {
	[PublicAPI]
	public class MySqlInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IRepository<MySqlConfiguration>>().To<MySqlConfigurationRepository>().InSingletonScope();
			kernel.Bind<IMySqlConfigurationService>().To<MySqlConfigurationService>().InSingletonScope();

            kernel.Bind<IDatabaseKeyRepository>().To<DatabaseKeyConfigRepository>().InSingletonScope();
            kernel.Bind<IDatabaseKeyResoulutionService>().To<DatabaseKeyResoulutionService>().InSingletonScope();


            kernel.Bind<IRepository<MySqlConnection>>().To<MySqlConnectionProvider>().InSingletonScope();

			kernel.Bind<IMySqlQueryService>().To<MySqlQueryService>().InSingletonScope();
		}
	}
}