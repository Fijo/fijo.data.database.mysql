using FijoCore.Data.Database.MySql.Dto;
using FijoCore.Data.Database.MySql.Interfaces;

namespace FijoCore.Data.Database.MySql.Services {
	public class MySqlConfigurationService : IMySqlConfigurationService {
		public string GetConnectionString(MySqlConfiguration configuration) {
			return string.Format(@"SERVER={0};Database={1};UID={2};PASSWORD={3}", configuration.Host, configuration.Database, configuration.User, configuration.Password);
		}	
	}
}