﻿using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Data.Database.MySql.Interfaces;
using MySql.Data.MySqlClient;

namespace FijoCore.Data.Database.MySql.Services
{
	public class MySqlQueryService : IMySqlQueryService {
		private readonly IRepository<MySqlConnection> _mySqlConnectionRepository;

		public MySqlQueryService(IRepository<MySqlConnection> mySqlConnectionRepository) {
			_mySqlConnectionRepository = mySqlConnectionRepository;
		}

		public long Insert(string mySqlQueryString) {
			var mySqlConnection = _mySqlConnectionRepository.Get();
			var myCommand = new MySqlCommand(mySqlQueryString) {Connection = mySqlConnection};
		    var executeNonQuery = myCommand.ExecuteNonQuery();
		    var lastInsertedId = executeNonQuery == 0 ? -1 : myCommand.LastInsertedId;
            mySqlConnection.Close();
		    return lastInsertedId;
		}

	    public bool Remove(string mySqlQueryString)
	    {
	        return Insert(mySqlQueryString) != -1;
	    }

	    public MySqlDataReader Query(string mySqlQueryString) {
			var command = _mySqlConnectionRepository.Get().CreateCommand();
			command.CommandText = mySqlQueryString;
			return command.ExecuteReader();
		}
	}
}