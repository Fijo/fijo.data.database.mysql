﻿using System.Collections.Generic;
using System.Linq;
using FijoCore.Data.Database.MySql.Interfaces;

namespace FijoCore.Data.Database.MySql.Services
{
    public class SqlQueryBuilder : ISqlQueryBuilder
    {
        private readonly IEntityFieldProvider _entityFieldProvider;
        private readonly string _all;
        private readonly string _insertOrUpdate;

        public SqlQueryBuilder(IEntityFieldProvider entityFieldProvider)
        {
            _entityFieldProvider = entityFieldProvider;
            var allFields = string.Join(", ", _entityFieldProvider.GetEntityFields().Select(x => x.Name));
            _all = string.Format("{0} FROM {1}", allFields, _entityFieldProvider.GetTableName());
            _insertOrUpdate = string.Format("INSERT INTO {0} ({1}) VALUES ({{0}}) ON DUPLICATE KEY UPDATE {2}",
                _entityFieldProvider.GetTableName(), allFields,
                string.Join(", ", _entityFieldProvider.GetEntityFields().Select(x => string.Format("{0} = VALUES({0})", x))));

        }

        public string GetQueryAllSql()
        {
            return string.Format("SELECT {0}", _all);
        }

        public string GetQueryByIdsSql(IEnumerable<string> key)
        {
            return string.Format("SELECT {0} WHERE {1} IN({2})", _all, _entityFieldProvider.GetPrimaryKey(), GetInPart(key));
        }

        public string GetInsertOrUpdateSql(IEnumerable<string> values)
        {
            return string.Format(_insertOrUpdate, string.Join(", ", values));
        }

        public string GetRemoveSql(IEnumerable<string> values)
        {
            return string.Format("DELETE {0} WHERE {1} IN({2})", _all, _entityFieldProvider.GetPrimaryKey(), GetInPart(values));
        }

        private string GetInPart(IEnumerable<string> key)
        {
            return string.Join(", ", key.Select(GetEscaped));
        }

        private string GetEscaped(string x)
        {
            return string.Format(@"""{0}""", x.Replace(@"""", @"\"""));
        }
    }
}