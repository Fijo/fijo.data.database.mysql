using FijoCore.Data.Database.MySql.Interfaces;

namespace FijoCore.Data.Database.MySql.Services {
    public class DatabaseKeyResoulutionService : IDatabaseKeyResoulutionService
    {
        private readonly IDatabaseKeyRepository _databaseKeyRepository;

        public DatabaseKeyResoulutionService(IDatabaseKeyRepository databaseKeyRepository)
        {
            _databaseKeyRepository = databaseKeyRepository;
        }

        public string Resolve(string databaseKey)
        {
            return _databaseKeyRepository.Get()[databaseKey];
        }	
	}
}