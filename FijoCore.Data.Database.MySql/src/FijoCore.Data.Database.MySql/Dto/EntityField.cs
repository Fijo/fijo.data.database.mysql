using System;

namespace FijoCore.Data.Database.MySql.Dto
{
    public class EntityField
    {
        public int Index { get; set; }
        public Type Type { get; set; }
        public Action<object, object> SetValue { get; set; }
        public Func<object, object> GetValue { get; set; }
        public string Name { get; set; }
        public bool IsPrimary { get; set; }
    }
}