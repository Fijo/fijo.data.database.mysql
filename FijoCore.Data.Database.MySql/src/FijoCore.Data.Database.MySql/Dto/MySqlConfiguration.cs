using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Data.Database.MySql.Dto {
	[Dto]
	public class MySqlConfiguration {
		public string Host { get; set; }
		public string Database { get; set; }
		public string User { get; set; }
		public string Password { get; set; }
	}
}