﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Fijo.Infrastructure.Model.DB.Attributes;
using FijoCore.Data.Database.MySql.Dto;
using FijoCore.Data.Database.MySql.Interfaces;
using FijoCore.Data.Database.MySql.Services;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;

namespace FijoCore.Data.Database.MySql.Repository
{
    public class EntityFieldProvider : IEntityFieldProvider
    {
        private readonly IList<EntityField> _entityFields;
        private readonly EntityField _primaryKey;
        private readonly string _tableName;
        private readonly string _database;
        private readonly Func<object> _constructor; 

        public EntityFieldProvider(Type entity, IDatabaseKeyResoulutionService databaseKeyResoulutionService)
        {
            var tableAttribute = GetTableAttribute(entity);
            _database = databaseKeyResoulutionService.Resolve(GetDatabaseKey(entity, tableAttribute));
            _tableName = tableAttribute.TableName;

            _entityFields = GetEntityFields(entity).ToList();
            _primaryKey = _entityFields.Single(x => x.IsPrimary);
            _constructor = GetConstructor(entity);
        }

        private IEnumerable<EntityField> GetEntityFields(Type entity)
        {
            var members = entity.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetField |
                                            BindingFlags.SetField | BindingFlags.GetProperty | BindingFlags.SetProperty);
            var index = 0;

            foreach (var member in members)
            {
                var fieldAttributes = member.GetCustomAttributes(typeof(FieldAttribute), false);
                if(!fieldAttributes.Any())
                    continue;
                var fieldAttribute = (FieldAttribute) fieldAttributes.Single();
                yield return new EntityField
                {
                    Index = index++,
                    Type = GetInstanceType(member),
                    SetValue = GetSetValueAction(member),
                    GetValue = GetGetValueFunc(member),
                    Name = fieldAttribute.Name,
                    IsPrimary = IsPrimaryKey(member)
                };
            }
        }

        private Func<object> GetConstructor(Type entity)
        {
            return () => Activator.CreateInstance(entity);
        } 

        private Action<object, object> GetSetValueAction(MemberInfo member)
        {
            var propertyInfo = member as PropertyInfo;

            if (propertyInfo != null)
                return (instance, value) => propertyInfo.SetValue(instance, value, null);

            return (instance, value) => ((FieldInfo) member).SetValue(instance, value);
        }

        private Func<object, object> GetGetValueFunc(MemberInfo member)
        {
            var propertyInfo = member as PropertyInfo;

            if (propertyInfo != null)
                return instance => propertyInfo.GetValue(instance, null);

            return instance => ((FieldInfo) member).GetValue(instance);
        }

        private Type GetInstanceType(MemberInfo member)
        {
            var propertyInfo = member as PropertyInfo;

            if (propertyInfo != null)
                return propertyInfo.PropertyType;

            return ((FieldInfo) member).FieldType;
        }

        private string GetDatabaseKey(Type entity, TableAttribute tableAttribute)
        {
            if (!string.IsNullOrEmpty(tableAttribute.DatabaseKey))
                return tableAttribute.DatabaseKey;

            var defaultDatabaseAttributes = entity.Assembly.GetCustomAttributes(typeof (DefaultDatabase), false);
            if (!defaultDatabaseAttributes.Any())
                return string.Empty;

            return ((DefaultDatabase) defaultDatabaseAttributes.Single()).DatabaseKey;
        }

        private TableAttribute GetTableAttribute(Type entity)
        {
            var tableAttributes = entity.GetCustomAttributes(typeof (TableAttribute), false);
#if DEBUG
            var tableAttributeCount = tableAttributes.Count();
            Debug.Assert(tableAttributeCount == 1,
                string.Format("An EntityType needs to have exactly one ´TableAttribute´´. The EntityType ´{0}´ has {1}.",
                    entity.FullName, tableAttributeCount));
#endif

            var tableAttribute = (TableAttribute) tableAttributes.Single();
            return tableAttribute;
        }

        private bool IsPrimaryKey(MemberInfo member)
        {
            return member.Name.ToLowerInvariant() == "id";
        }

        public IList<EntityField> GetEntityFields()
        {
            return _entityFields;
        }

        public object CreateInstance()
        {
            return _constructor();
        }

        public EntityField GetPrimaryKey()
        {
            return _primaryKey;
        }

        public string GetTableName()
        {
            return _tableName;
        }

        public string GetDatabase()
        {
            return _database;
        }
    }
}