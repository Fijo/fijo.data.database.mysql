﻿using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Data.Database.MySql.Interfaces;
using MySql.Data.MySqlClient;
using MySqlConfiguration = FijoCore.Data.Database.MySql.Dto.MySqlConfiguration;

namespace FijoCore.Data.Database.MySql.Repository
{
	public class MySqlConnectionProvider : RepositoryBase<MySqlConnection>
	{
		private readonly string _getConnectionString;

		public MySqlConnectionProvider(IRepository<MySqlConfiguration> mySqlConfigurationRepository, IMySqlConfigurationService mySqlConfigurationService) {
			_getConnectionString = mySqlConfigurationService.GetConnectionString(mySqlConfigurationRepository.Get());
		}

		#region Overrides of RepositoryBase<MySqlConnection>
		public override MySqlConnection Get() {
			var connection = new MySqlConnection(_getConnectionString);
			connection.Open();
			return connection;
		}
		#endregion
	}
}