using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Data.Database.MySql.Interfaces;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace FijoCore.Data.Database.MySql.Repository
{
    public class DatabaseKeyConfigRepository : SingletonRepositoryBase<IDictionary<string, string>>, IDatabaseKeyRepository 
    {
        private readonly IConfigurationService _configurationService;

        public DatabaseKeyConfigRepository(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        protected override IDictionary<string, string> Create()
        {
            return _configurationService.Get<Dictionary<string, string>, DatabaseKeyConfigRepository>("Databases");
        }
    }
}