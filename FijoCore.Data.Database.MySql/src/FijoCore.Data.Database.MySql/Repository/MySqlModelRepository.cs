﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Data.Database.MySql.Interfaces;
using FijoCore.Data.Database.MySql.Services;
using MySql.Data.MySqlClient;

namespace FijoCore.Data.Database.MySql.Repository
{
    public class MySqlModelRepository<TEntity, TKey> : CollectionRepositoryBase<TEntity>, IMySqlRepository<TEntity, TKey>
    {
        protected readonly IMySqlQueryService QueryService;
        protected readonly ISqlQueryBuilder SqlQueryBuilder;
        private readonly EntityFieldProvider _entityFieldProvider;

        public MySqlModelRepository(IMySqlQueryService queryService, IDatabaseKeyResoulutionService databaseKeyResoulutionService)
        {
            QueryService = queryService;
            _entityFieldProvider = CreateEntityFieldProvider(databaseKeyResoulutionService);
            SqlQueryBuilder = CreateSqlQueryBuilder(databaseKeyResoulutionService, _entityFieldProvider);
        }

        protected virtual ISqlQueryBuilder CreateSqlQueryBuilder(IDatabaseKeyResoulutionService databaseKeyResoulutionService, EntityFieldProvider createEntityFieldProvider)
        {
            return new SqlQueryBuilder(createEntityFieldProvider);
        }

        protected virtual EntityFieldProvider CreateEntityFieldProvider(IDatabaseKeyResoulutionService databaseKeyResoulutionService)
        {
            return new EntityFieldProvider(typeof(TEntity), databaseKeyResoulutionService);
        }

        protected override IEnumerable<TEntity> GetAll()
        {
            return ReadAllToInstances(QueryService.Query(SqlQueryBuilder.GetQueryAllSql()));
        }

        private IEnumerable<TEntity> ReadAllToInstances(MySqlDataReader mySqlDataReader)
        {
            using (mySqlDataReader)
                while (mySqlDataReader.Read())
                    yield return ReadToInstance(mySqlDataReader);
        }

        private TEntity ReadToInstance(MySqlDataReader mySqlDataReader)
        {
            var instance = (TEntity) _entityFieldProvider.CreateInstance();

            foreach (var entityField in _entityFieldProvider.GetEntityFields())
                entityField.SetValue(instance, mySqlDataReader[entityField.Name]);
            return instance;
        }

        public TKey Set(TEntity entity)
        {
            return InternalSet(new[] { entity });
        }

        public void Set(IList<TEntity> collection)
        {
            // not sure if every row succeded so I dont wana give back a last insert id
            InternalSet(collection);
        }

        private TKey InternalSet(IList<TEntity> collection)
        {
            return (TKey) (object) QueryService.Insert(SqlQueryBuilder.GetInsertOrUpdateSql(collection.Select(x => string.Format("({0})", string.Join(", ", _entityFieldProvider.GetEntityFields().Select(y => GetEscaped(y.GetValue(x).ToString())))))));
        }

        public bool Remove(TKey key)
        {
            return InternalRemove(new[] { key });
        }

        public void Remove(IList<TKey> collection)
        {
            // not sure if every row succeded so I dont wana give wether the it did
            InternalRemove(collection);
        }

        private bool InternalRemove(IList<TKey> collection)
        {
            return QueryService.Remove(SqlQueryBuilder.GetRemoveSql(collection.Select(KeyToString)));
        }

        public TEntity Get(TKey id)
        {
            return Get(new[] {id}).FirstOrDefault();
        }

        public IEnumerable<TEntity> Get(IList<TKey> collection)
        {
            return ReadAllToInstances(QueryService.Query(SqlQueryBuilder.GetQueryByIdsSql(collection.Select(KeyToString))));
        }

        private string GetEscaped(string x)
        {
            return string.Format(@"""{0}""", x.Replace(@"""", @"\"""));
        }

        private string KeyToString(TKey arg)
        {
            Debug.Assert(arg is int); // meh
            return arg.ToString();
        }
    }
}
