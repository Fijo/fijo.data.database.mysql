using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Data.Database.MySql.Dto;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace FijoCore.Data.Database.MySql.Repository {
	public class MySqlConfigurationRepository : SingletonRepositoryBase<MySqlConfiguration> {
		private readonly IConfigurationService _configurationService;

		public MySqlConfigurationRepository(IConfigurationService configurationService) {
			_configurationService = configurationService;
		}

		#region Overrides of SingletonRepositoryBase<MySqlConfiguration>
		protected override MySqlConfiguration Create() {
			return new MySqlConfiguration
			       {
				       Host = _configurationService.Get<string, MySqlConfigurationRepository>(CN.Get<MySqlConfiguration, string>(x => x.Host)),
				       Database = _configurationService.Get<string, MySqlConfigurationRepository>(CN.Get<MySqlConfiguration, string>(x => x.Database)),
				       User = _configurationService.Get<string, MySqlConfigurationRepository>(CN.Get<MySqlConfiguration, string>(x => x.User)),
				       Password = _configurationService.Get<string, MySqlConfigurationRepository>(CN.Get<MySqlConfiguration, string>(x => x.Password))
			       };
		}
		#endregion
	}
}